<?php
use TestProject\User;
use TestProject\Quiz;
require_once 'init.php';

$data_arr = Quiz::getResult( $_GET['key'] );

$quiz = new Quiz( $data_arr['quiz'] );
$user = new User( $data_arr['user'] );

$maxScore_int = $quiz->getMaxScore();

$scorePer_num   = $data_arr['score'] / $maxScore_int * 100;
$averagePer_num = $data_arr['average'] / $maxScore_int * 100;

if ( $scorePer_num > $averagePer_num + 10 ){
	$barColour_str = "bg-success";
} elseif ( $scorePer_num < $averagePer_num - 10 ){
	$barColour_str = "bg-danger";
} else {
	$barColour_str = "bg-warning";
}

?>
<!doctype html>
<html lang=en_GB>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Basic PHP Quiz</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="text-center">Results</h1>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h2 class="text-center">Thank you, <?php echo $user->getName(); ?><br /><?php echo $quiz->getName(); ?></h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<h3>Your score</h3>
					<div class="progress">
						<div class="progress-bar <?php echo $barColour_str; ?>" role="progressbar" style="width: <?php echo $scorePer_num; ?>%"><?php echo $data_arr['score'] . " / " . $maxScore_int ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<h3>Average score</h3>
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: <?php echo $averagePer_num; ?>%"><?php echo round( $data_arr['average'], 1 ) . " / " . $maxScore_int ?></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<a href="./" class="mt-3 btn btn-primary">Return to home</a>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>
