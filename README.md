# Basic PHP Quiz system

A simple system for multiple choice quizes.

N.B. While the system will allow a user to take each quiz multiple time and save the total for each attempt, individual question scores are only saved for the latest attempt.

# Installation

Import the included dump file, copy `config.dist.php` to `config.php` and update the connection details

# Answers to example quizzes

## Quiz A

1. B
2. A
3. False
4. A and C

## Quiz B

1. True
2. D
3. C and D
4. True
5. False
6. B
