-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 06, 2019 at 04:53 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `correct` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `question`, `text`, `correct`) VALUES
(1, 1, 'A', 0),
(2, 1, 'B', 1),
(3, 1, 'C', 0),
(4, 1, 'D', 0),
(5, 2, 'A', 1),
(6, 2, 'B', 0),
(7, 2, 'C', 0),
(8, 2, 'D', 0),
(9, 3, 'True', 0),
(10, 3, 'False', 1),
(11, 4, 'A', 1),
(12, 4, 'B', 0),
(13, 4, 'C', 1),
(14, 4, 'D', 0),
(15, 5, 'True', 1),
(16, 5, 'False', 0),
(17, 6, 'A', 0),
(18, 6, 'B', 0),
(19, 6, 'C', 0),
(20, 6, 'D', 1),
(21, 7, 'A', 0),
(22, 7, 'B', 0),
(23, 7, 'C', 1),
(24, 7, 'D', 1),
(25, 8, 'True', 1),
(26, 8, 'False', 0),
(27, 9, 'True', 0),
(28, 9, 'False', 1),
(29, 10, 'A', 0),
(30, 10, 'B', 1),
(31, 10, 'C', 0),
(32, 10, 'D', 0);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `quiz` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `quiz`, `text`) VALUES
(1, 1, 'Question 1'),
(2, 1, 'Question 2'),
(3, 1, 'Question 3'),
(4, 1, 'Question 4'),
(5, 2, 'Question 1'),
(6, 2, 'Question 2'),
(7, 2, 'Question 3'),
(8, 2, 'Question 4'),
(9, 2, 'Question 5'),
(10, 2, 'Question 6');

-- --------------------------------------------------------

--
-- Table structure for table `question_result`
--

CREATE TABLE `question_result` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `question_result`
--

INSERT INTO `question_result` (`id`, `user`, `question`, `score`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 4, 1),
(5, 1, 5, 1),
(6, 1, 6, 0),
(7, 1, 7, 2),
(8, 1, 8, 1),
(9, 1, 9, 1),
(10, 1, 10, 0),
(11, 2, 1, 1),
(12, 2, 2, 0),
(13, 2, 3, 1),
(14, 2, 4, 1),
(15, 2, 5, 1),
(16, 2, 6, 1),
(17, 2, 7, 2),
(18, 2, 8, 1),
(19, 2, 9, 1),
(20, 2, 10, 1),
(21, 3, 1, 1),
(22, 3, 2, 1),
(23, 3, 3, 1),
(24, 3, 4, 2),
(25, 3, 5, 1),
(26, 3, 6, 0),
(27, 3, 7, 1),
(28, 3, 8, 1),
(29, 3, 9, 1),
(30, 3, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(11) NOT NULL,
  `name` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `max_score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `name`, `max_score`) VALUES
(1, 'Quiz A', 5),
(2, 'Quiz B', 7);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_result`
--

CREATE TABLE `quiz_result` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `quiz` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `access_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `quiz_result`
--

INSERT INTO `quiz_result` (`id`, `user`, `quiz`, `score`, `access_key`) VALUES
(1, 1, 1, 3, 'b652c5197ff6a925207ec3918fa48c8b7a784a04f1c138d854f2fe0cc97683f7'),
(2, 1, 1, 4, 'dd58163bbfd01019db2282a650c54740c15fe82538cb4e139616a01a193bc2a2'),
(3, 1, 1, 4, 'f00e8ade60388d07a9b41db2cbfc717ca6ba26f0303b21ae2432e3f73e0b5232'),
(4, 1, 1, 4, '4312387976c6d31ee8db2b489e5b1c15ff201ad0fb9e78ebc86943256a6f3e90'),
(5, 1, 1, 4, 'f9acce8335ce94d53f59abb619b130e9fc4b3c91ac7d15934ddc574eead8b372'),
(6, 1, 2, 5, 'fa8b41c6c18c869384757cd098b3d8a5ecd8ca1b8fc873d4f7e793e8e832fdc2'),
(7, 2, 1, 3, 'e1d70bced30ebbdd1aca892e356aca550bd6b1834bf26fef7c81c7a8cf3fd662'),
(8, 2, 2, 5, 'f4cd01f61a141e6e17861217ef7e1629d9a43eb43408b7baec7629c2505ff80c'),
(9, 3, 1, 0, '43633a14bfc30214b9d87dcb9fbbd6cc50585202c7ded0c92377a54292a40f4d'),
(10, 3, 1, 5, 'eeaa8d665f14b5d867522566cca267f92af3c3b466723f1060bc14e9e033c1a0'),
(11, 3, 2, 4, '4fed14a306c950d263a25064b6c498f9a1e7972466e9e8c7e7a76815c566653b'),
(12, 2, 2, 7, '3552225568e1df9df4f2e819b9603c2ac450d5618df8cb740e42fc8414d44cf7');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`) VALUES
(1, 'mark@cool110.xyz', 'Mark'),
(2, 'test@cool110.xyz', 'Test User'),
(3, 'test2@cool110.xyz', 'Test User 2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question` (`question`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz` (`quiz`);

--
-- Indexes for table `question_result`
--
ALTER TABLE `question_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `question` (`question`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_result`
--
ALTER TABLE `quiz_result`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_key` (`access_key`),
  ADD KEY `user` (`user`),
  ADD KEY `quiz` (`quiz`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `question_result`
--
ALTER TABLE `question_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `quiz_result`
--
ALTER TABLE `quiz_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`question`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`quiz`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_result`
--
ALTER TABLE `question_result`
  ADD CONSTRAINT `question_result_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `question_result_ibfk_2` FOREIGN KEY (`question`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quiz_result`
--
ALTER TABLE `quiz_result`
  ADD CONSTRAINT `quiz_result_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quiz_result_ibfk_2` FOREIGN KEY (`quiz`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
