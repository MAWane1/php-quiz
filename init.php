<?php
session_start();
require_once 'config.php';

spl_autoload_register( function( $class_name ){
	$class_name = str_replace( 'TestProject\\', 'classes/', $class_name );
	include strtolower( $class_name ) . '.class.php';
} );
