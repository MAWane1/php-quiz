<?php
use TestProject\User;
use TestProject\Quiz;
use TestProject\Question;
require_once 'init.php';

$quiz  = new Quiz( $_POST['quiz'] );
$quest = new Question( $_POST['quest'] );

$quest->saveAnswer( $_POST['user'], $_POST['ans'] );

$questID_arr = Question::getList( $quiz->getID() );
$index_int   = array_search( $quest->getID(), $questID_arr );

$index_int++;
if ( count( $questID_arr ) > $index_int ){
	$nextQuest = new Question( $questID_arr[ $index_int ] );
	$ans_arr   = $nextQuest->getAnswers();

	$data_arr = array(
		'done' => false,
		'text' => $nextQuest->getText(),
		'ans'  => array()
	);

	foreach( $ans_arr as $answer ){
		$data_arr['ans'][] = array( 'id' => $answer->getID(), 'text' => $answer->getText() );
	}
} else {
	$user = new User( $_POST['user'] );

	$accessKey_str = $quiz->saveResult( $_POST['user'] );

	$user->emailResult( $accessKey_str );

	$data_arr = array(
		'done'   => true,
		'result' => $accessKey_str
	);
}

header( "content-type: application/json" );
echo json_encode( $data_arr );
