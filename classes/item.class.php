<?php
namespace TestProject;

/**
 * Item class
 */
class Item {
	protected $id_int;

	/**
	 * Get ID of Item
	 *
	 * @return int ID
	 */
	public function getID(){
		return $this->id_int;
	}
}
