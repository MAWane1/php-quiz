<?php
namespace TestProject;

/*
 * Answer class
 */
 class Answer extends Item {
	private const tb_str = 'answer';

	private $text_str;
	private $correct_bln;

	/**
	 * Load answer
	 *
	 * @param int $id_int ID of answer to load
	 */
	public function __construct( int $id_int ){
		$db = DB::getDB();

		$rows_arr = $db->select( self::tb_str, array(), array( 'id' => $id_int ) );

		if ( empty( $rows_arr ) ){
			throw new \RuntimeException( 'Invalid answer ID' );
		}

		$this->id_int      = $rows_arr[0]['id'];
		$this->text_str    = $rows_arr[0]['text'];
		$this->correct_bln = (bool) $rows_arr[0]['correct'];
	}

	/**
	 * Get a list of answer IDs for a question
	 *
	 * @param int $questID_int ID of question to load answers for
	 *
	 * @return int[] Array of answer IDs
	 */
	public static function getList( int $questID_int ){
		$db = DB::getDB();

		$rows_arr = $db->select( self::tb_str, array( 'id' ), array( 'question' => $questID_int ) );

		$id_arr = array();
		foreach( $rows_arr as $row_arr ){
			$id_arr[] = $row_arr['id'];
		}

		return $id_arr;
	}

	/**
	 * Get question text
	 *
	 * @return string Question text
	 */
	public function getText(){
		return $this->text_str;
	}

	/**
	 * Get if answer is correct
	 *
	 * @return bool if is correct
	 */
	public function isCorrect(){
		return $this->correct_bln;
	}

}
