<?php
namespace TestProject;

/*
 * User class
 */
class User extends Item {
	private const tb_str = 'user';

	private $name_str;
	private $email_str;

	/**
	 * Load user
	 *
	 * @param int $id_int ID of user to load
	 */
	public function __construct( int $id_int ){
		$db = DB::getDB();

		$rows_arr = $db->select( self::tb_str, array(), array( 'id' => $id_int ) );

		if ( empty( $rows_arr ) ){
			throw new \RuntimeException( 'Invalid user ID' );
		}

		$this->id_int    = $rows_arr[0]['id'];
		$this->name_str  = $rows_arr[0]['name'];
		$this->email_str = $rows_arr[0]['email'];
	}

	/**
	 * Find user by email and optionally update name
	 *
	 * @param string $email_str Email to search by
	 * @param string $name_str Name of user to update if needed
	 *
	 * @return TestProject\User User object found or created
	 */
	public static function find( string $email_str, string $name_str = '' ){
		$db = DB::getDB();

		$rows_arr = $db->select( self::tb_str, array( 'id' ), array( 'email' => $email_str ) );

		if ( empty( $rows_arr ) ){
			$id_int = self::create( $email_str, $name_str );
		} else {
			$id_int = $rows_arr[0]['id'];
		}

		$user = new User( $id_int );

		if ( ! empty( $name_str ) && $user->getName() != $name_str ){
			$user->updateName( $name_str );
		}

		return $user;
	}

	/**
	 * Create a new user
	 *
	 * @param string $email_str Email of user
	 * @param string $name_str Name of user
	 *
	 * @return int ID of new user
	 */
	private static function create( string $email_str, string $name_str ){
		$db = DB::getDB();

		$db->insert( self::tb_str, array( 'email' => $email_str, 'name' => $name_str ) );

		$rows_arr = $db->select( self::tb_str, array( 'id' ), array( 'email' => $email_str ) );

		return $rows_arr [0]['id'];
	}

	/**
	 * Update name of user
	 *
	 * @param string $name New name
	 */
	private function updateName( string $name_str ){
		$db = DB::getDB();

		$db->update( self::tb_str, array( 'name' => $name_str ), array( 'id' => $this->id_int ) );

		$this->name_str = $name_str;
	}

	/**
	 * Get name of User
	 *
	 * @return string Name
	 */
	public function getName(){
		return $this->name_str;
	}

	/**
	 * Get email of User
	 *
	 * @return string Email
	 */
	public function getEmail(){
		return $this->email_str;
	}

	/**
	 * Email a link to the results page
	 *
	 * @param string $accessKey_str Access key for result
	 */
	public function emailResult( string $accessKey_str ){
		$to_str      = $this->name_str . " <" . $this->email_str . ">";
		$subject_str = "Your results";
		$url_str     = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME'] . dirname( $_SERVER['PHP_SELF'] ) . "/result.php?key="  . $accessKey_str;
		$body_str    = "Thank you for completing the quiz, your result can be viewed at the following link. \r\n" . $url_str;

		mail( $to_str, $subject_str, $body_str );
	}
}
