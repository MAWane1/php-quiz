<?php
namespace TestProject;

/**
 * Database abstraction class
 */
class DB {
	private static $instance = NULL;

	private $conn;

	/**
	 * Establish database connection using details in config file.
	 */
	private function __construct(){
		$dsn_str = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;

		$this->conn = new \PDO( $dsn_str, DB_USER, DB_PASS );
	}

	/**
	 * Get instance of DB class
	 *
	 * @return TestProject\DB class instance
	 */
	public static function getDB(){
		if ( self::$instance == NULL ){
			self::$instance = new DB();
		}

		return self::$instance;
	}

	/**
	 * General select method
	 *
	 * Assumes table and columns names are sanitised without surrounding `
	 *
	 * @param string $table_str Table to select from
	 * @param mixed[] $fields_arr Columns to select, omit to select all
	 * @param mixed[] $search_arr Data to search by, omit to select all rows
	 *
	 * @return mixed[] Array of selected rows
	 */
	public function select( string $table_str, $fields_arr = array(), $search_arr = array()  ){
		if ( empty( $fields_arr ) ){
			$fields_str = '*';
		} else {
			foreach( $fields_arr as $field_str ){
				$field_str = '`' . $field_str . '`';
			}
			$fields_str = implode( ', ', $fields_arr );
		}

		$val_arr = array();
		if ( empty( $search_arr ) ){
			$search_str = '';
		} else {
			$search_str = 'WHERE ';
			$first_bln = true;
			foreach( $search_arr as $field_str => $value ){
				if ( ! $first_bln ){
					$search_str .= ' AND ';
				}
				$first_bln = false;

				$search_str .= '`' . $field_str . '` = ?';

				$val_arr[] = $value;
			}
		}

		$sql_str = 'SELECT ' . $fields_str . ' FROM `' . $table_str . '` ' . $search_str;

		$stmt = $this->conn->prepare( $sql_str );
		$stmt->execute( $val_arr );

		$result_arr = $stmt->fetchAll( \PDO::FETCH_ASSOC );

		return $result_arr;
	}

	/**
	 * General insert method
	 *
	 * Assumes table and columns names are sanitised without surrounding `
	 *
	 * @param string $table_str Table to insert into
	 * @param mixed[] $data_arr Data to insert
	 *
	 * @return bool Success status
	 */
	public function insert( string $table_str, $data_arr ){
		$fields_str = '';
		$val_arr = array();

		$first_bln = true;
		foreach ( $data_arr as $field_str => $value ){
			if ( ! $first_bln ){
				$fields_str .= ', ';
			}
			$first_bln = false;

			$fields_str .= '`' . $field_str . '`';

			$val_arr[] = $value;
		}

		$sql_str = 'INSERT INTO `' . $table_str . '` (' . $fields_str . ') VALUES (' . implode( ', ', array_fill( 0, count( $val_arr ), '?' ) ) . ')';

		$stmt = $this->conn->prepare( $sql_str );
		return $stmt->execute( $val_arr );
	}

	/**
	 * General update method
	 *
	 * Assumes table and columns names are sanitised without surrounding `
	 *
	 * @param string $table_str Table to update
	 * @param mixed[] $data_arr Data to update
	 * @param mixed[] $search_arr Data to select row to update
	 *
	 * @return bool Success status
	 */
	public function update( string $table_str, $data_arr, $search_arr ){
		$fields_str = '';
		$search_str = '';
		$var_arr = array();

		$first_bln = true;
		foreach ( $data_arr as $field_str => $value ){
			if ( ! $first_bln ){
				$fields_str .= ', ';
			}
			$first_bln = false;

			$fields_str .= '`'. $field_str . '` = ?';

			$val_arr[] = $value;
		}

		$first_bln = true;
		foreach ( $search_arr as $field_str => $value ){
			if ( ! $first_bln ){
				$search_str .= ' AND ';
			}
			$first_bln = false;

			$search_str .= '`' . $field_str . '` = ?';

			$val_arr[] = $value;
		}

		$sql_str = 'UPDATE `' . $table_str .'` SET ' . $fields_str . ' WHERE ' . $search_str;

		$stmt = $this->conn->prepare( $sql_str );
		return $stmt->execute( $val_arr );

	}
}
