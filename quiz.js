$( document ).ready( function() {
	"use strict";

	let questNum = 1;

	$( "#question" ).on( "submit", function( event ){
		event.preventDefault();
		let ans = [];
		$( "[name='answer']:checked" ).each( function( index, element ){
			ans[ ans.length ] = element.value;
		} );

		$.post( 'ajax.php', {
			user  : id.user,
			quiz  : id.quiz,
			quest : id.quest[ questNum - 1],
			ans   : ans,
		} ).done( function( data ){
			if ( data.done ){
				// Send user to results page
				window.location.replace( "result.php?key=" + data.result );
			} else {
				// Display next question
				questNum++;
				$( "#question-text" ).text( data.text );
				$( "#answers" ).empty();

				$.each( data.ans, function( index, value ){
					$( "#answers" ).append( '\
					<div class="col-md-6">\
						<label class="w-100">\
							<input type="checkbox" name="answer" value="' + value.id + '" />\
							' + value.text + '\
						</label>\
					</div>' );
				} );

				let progress = questNum / id.quest.length * 100;
				$( "#progress" ).text( questNum + ' / ' + id.quest.length ).prop( "style", "width: " + progress + "%" );
			}
		} );
	} );
} );
