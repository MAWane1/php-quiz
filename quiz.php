<?php
use TestProject\User;
use TestProject\Quiz;
use TestProject\Question;
require_once 'init.php';

$user = User::find( $_POST['email'], $_POST['name'] );
$quiz = new Quiz( $_POST['quiz'] );

$_SESSION['user_id'] = $user->getID();

$questID_arr = Question::getList( $quiz->getID() );

$quest1 = new Question( $questID_arr[0] );

$progress_num = 1 / count( $questID_arr ) * 100;
?>
<html lang=en_GB>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Basic PHP Quiz</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script>
			let id = {
				user  : <?php echo $user->getID(); ?>,
				quiz  : <?php echo $quiz->getID(); ?>,
				quest : <?php echo json_encode( $questID_arr ); ?>
			};
		</script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="text-center"><?php echo $quiz->getName(); ?></h1>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h2 class="text-center" id="question-text"><?php echo $quest1->getText(); ?></h2>
				</div>
			</div>
			<form id="question">
				<div class="form-row" id="answers">
				<?php foreach( $quest1->getAnswers() as $answer ){
					$id   = $answer->getID();
					$text = $answer->getText();
				?>
					<div class="col-md-6">
						<label class="w-100">
							<input type="checkbox" name="answer" value="<?php echo $id; ?>" />
							<?php echo $text; ?>
						</label>
					</div>
				<?php } ?>
				</div>
				<div class="form-row">
					<div class="col">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
			<div class="row">
				<div class="col">
					<div class="progress">
						<div id="progress" class="progress-bar" role="progressbar" style="width: <?php echo $progress_num; ?>%">1 / <?php echo count( $questID_arr ); ?></div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="quiz.js"></script>
	</body>
</html>
