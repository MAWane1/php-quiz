<?php
use TestProject\User;
use TestProject\Quiz;
require_once 'init.php';

if ( ! empty( $_SESSION['user_id'] ) ){
	$user = new User( $_SESSION['user_id'] );
	$name_str   = $user->getName();
	$email_str = $user->getEmail();
}

$quizzes_arr = Quiz::getList();
?>
<!doctype html>
<html lang=en_GB>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Basic PHP Quiz</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<header class="row">
				<div class="col">
					<h1 class="text-center">Basic PHP Quiz</h1>
				</div>
			</header>
			<main>
				<form method="POST" action="quiz.php">
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="name">Name</label>
							<input class="form-control" id="name" name="name" type="text" value="<?php echo $name_str ?? ''; ?>" />
						</div>
						<div class="form-group col-md-6">
							<label for="email">Email<span class="text-danger">*</span></label>
							<input class="form-control" id="email" name="email" type="email" value="<?php echo $email_str ?? ''; ?>" required />
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col">
							<label for="quiz">Select quiz</label>
							<select class="form-control" id="quiz" name="quiz">
							<?php foreach( $quizzes_arr as $quiz_arr ){
								echo '<option value="' . $quiz_arr['id'] . '">' . $quiz_arr['name'] . '</option>';
							} ?>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<button type="submit" class="btn btn-primary">Start</button>
						</div>
					</div>
				</form>
			</main>
		</div>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>
